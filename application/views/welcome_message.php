	<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
		<link rel="stylesheet" type="text/css" href=" https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
		<!-- jQuery -->
		<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
		<!-- DataTables -->
		<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

	</head>
	<body>
		<form>
			<input type="text" class="date-own form-control" id="start_date" name="start_date" placeholder="" value="" required>
			<input type="text" class="date-own form-control" id="end_date" name="end_date" placeholder="" value="" required>
			<button type="button" name="search" id="search">Cari</button>
		</form>

		<div class="row">
	    <div class="col-md-12 offset-md-2">
	        <h4 class="mb-3">Data Paling Banyak Di Lihat</h4>
					<table id="dataya" class="mdl-data-table" style="width:100%">
            <thead>
							<tr>
								<th>Tahun</th>
								<th>Judul</th>
								<th>Kategori</th>
								<th>Viewer</th>
							</tr>
            </thead>
            <tbody>
            </tbody>
        </table>
	    </div>


		<script type="text/javascript">
    $(document).ready(function() {
			$('#dataya').DataTable({
				 "order": [[ 0, "desc" ]],
				 lengthChange: false,
				 ajax: {
						 url: "http://localhost/jdih_webservice/index.php/api/dokumen",
						 dataSrc: ""
				 },
				 columns: [
						 { data: "thntetap"},{ data: "judul"},{ data: "singkatan"},{ data: "dilihat"}
				 ],
				 select: true
			});
        $('#search').click(function(){
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
					var myTable = $('#dataya').DataTable({
		        "paging": true, "lengthChange": true,
		        "searching": true,"ordering": true,"info": true,
						"bDestroy": true,"autoWidth": true, "data": [],
		        "columns": [
							{"title": "Tahun","data": "thntetap"},
							{"title": "Judul","data": "judul"},
							{"title": "Kategori","data": "singkatan"},
							{"title": "Viewer","data": "dilihat"}
						],
						"order": [[ 0, "desc" ]]
		    });

          if(start_date != '' && end_date !='')
          {
            var startDate = new Date(start_date).getFullYear();
            var endDate = new Date(end_date).getFullYear();
            let url = 'http://localhost/jdih_webservice/index.php/api/dokumen';
            fetch(url)
            .then(res => res.json())
            .then((out) => {
                var resultProductData = out.filter(function(a) {
                var createdAt = new Date(a.tgltetap).getFullYear();
                  if( createdAt >= startDate && createdAt <= endDate ) return a;
                });
								myTable.clear();
								$.each(resultProductData, function (index, value) {
						      myTable.row.add(value);
						    });
						    myTable.draw();
                console.log(resultProductData);
            })
            .catch(err => { throw err });

          }
          else
          {
            alert("Both Date is Required");
          }
        });
      });

    </script>
	</body>
</html>
